﻿using UnityEngine;
using System.Collections;

public class MainMenuSceneFrameController : MonoBehaviour {
    public RectTransform topFrame, bottomFrame;         //2 gameobjects to control top and bot parts of frame
    protected float frameWidth;                         //horizontal size of frame which have to be filled
    protected float horizontalFrameSize;                //size of 1 part of horizontal frame
    public Transform frame;                             //object which will be parent for new borders copies
    protected const float CAMERA_HEIGHT_DOUBLED=10.0f;    //const value of camera height doubled

	void Start () {
        frameWidth = CAMERA_HEIGHT_DOUBLED * Screen.width / Screen.height;                              //simple proportion, 10 is const of height took form camera     
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(frameWidth, CAMERA_HEIGHT_DOUBLED);  //changing frame gameobject width
        horizontalFrameSize = topFrame.sizeDelta.x * topFrame.localScale.x; //calculating size of 1 part of horizontal frame

        //is one part enough to fill all horizontal part of frame
        if (frameWidth > horizontalFrameSize)                                           
        {        
            float numberOfAdditionalTextures = Mathf.Floor(frameWidth / horizontalFrameSize * 1.0f);                //calculating how much copies we need
            int isFlipped = 1;                                                                                      //int variable to contol flipping for very wide screen
            float topBoardShift = topFrame.transform.localPosition.x;
            float bottomBoardShift = bottomFrame.transform.localPosition.x;                                         //2 variables to save position of previous texture
            for (int i = 0; i < numberOfAdditionalTextures; i++)
            {
                //Creating top and bot copies with necessary position, with mirror reflection effect and as child of Frame
                CreateFrameSprite(topFrame, topBoardShift, isFlipped);                                                    
                CreateFrameSprite(bottomFrame, bottomBoardShift, isFlipped);

                isFlipped+=1;                                                                                       //bool var was changed to int - more ram and cpu usage but no necessary to make if..else construction 
                isFlipped = isFlipped % 2;

                topBoardShift += horizontalFrameSize;                                                               //increment shifting for one sprite width
                bottomBoardShift += horizontalFrameSize;
            }
        }
	}

    //this function instantiate only one frame sprite at one position
    protected void CreateFrameSprite(RectTransform sprite, float leftBoardShift, int isFlipped)
    {
        Instantiate(sprite, new Vector3(leftBoardShift + horizontalFrameSize, sprite.transform.localPosition.y, 90), new Quaternion(0, isFlipped, 0, 0), frame);
    }
}