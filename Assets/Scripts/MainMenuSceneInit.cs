﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuSceneInit : MonoBehaviour {    
    public static int fontSize;
    public static int spacingSize;
    protected const int DEFAULT_FONT_SIZE = 40;
    protected const int DEFAULT_SPACING_COEFFICIENT = 8;
	
    //also in future there are can be some features like translation/some data between scenes/some settings
	void Start () {
        //if not set in editor
        if (fontSize == 0)
        {    
            fontSize = DEFAULT_FONT_SIZE;
        }
        if (spacingSize == 0)
        {
            spacingSize = DEFAULT_SPACING_COEFFICIENT;
        }
        //adapting functions
        MainMenuSceneTags.FontSizeAdapter();
        MainMenuSceneTags.LayoutSpacingAdapter();
	}
}

