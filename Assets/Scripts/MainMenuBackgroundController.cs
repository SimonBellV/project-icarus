﻿using UnityEngine;
using System.Collections;

public class MainMenuBackgroundController : MonoBehaviour {
    public SpriteRenderer backgroundSprite;         //green board
    public SpriteRenderer moodSprite;               //mood effect
    protected const float CAMERA_HEIGHT = 5.0f;      //const value of camera height           

	void Start () 
    {   
        float newLocaleScale;                       //tempo variable to calculate value of scaling
        float backgroundSpriteProportion;           //contains sprite proportion W/H
        float screenResolutionProportion = 1.0f*Screen.width / Screen.height;//there should be 1.0f cause int/int=int but float*int/int = float
        Vector3 backgroundSpriteBorders;            //green board borders
        Vector3 moodSpriteBorders;                  //mood sprite borders
        backgroundSpriteBorders = backgroundSprite.bounds.extents;
        moodSpriteBorders = moodSprite.bounds.extents;
        backgroundSpriteProportion = backgroundSpriteBorders.x / backgroundSpriteBorders.y;

        if (backgroundSpriteProportion < screenResolutionProportion)
        {
            //scaling background by horizontal
            float var = CAMERA_HEIGHT * screenResolutionProportion;
            newLocaleScale = var / backgroundSpriteBorders.x;
            backgroundSprite.transform.localScale = new Vector3(var / backgroundSpriteBorders.x, var / backgroundSpriteBorders.x, 0);
            newLocaleScale = var / moodSpriteBorders.x;
            moodSprite.transform.localScale = new Vector3(newLocaleScale,newLocaleScale, 0);
        }
        else
        {
            //scaling background by vertical
            newLocaleScale = CAMERA_HEIGHT / backgroundSpriteBorders.y;
            backgroundSprite.transform.localScale = new Vector3(newLocaleScale, newLocaleScale, 0);
            newLocaleScale = CAMERA_HEIGHT / moodSpriteBorders.y;
            moodSprite.transform.localScale = new Vector3(newLocaleScale, newLocaleScale, 0);
        }
	}
}
