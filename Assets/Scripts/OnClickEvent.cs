﻿using UnityEngine;
using System.Collections;

public class OnClickEvent : MonoBehaviour {
    
    public void OnPlayButtonClick()
    {
        Debug.Log("Pressed Play");
    }

    public void OnProfileButtonClick()
    {
        Debug.Log("Pressed Profile");
    }

    public void OnQuestsButtonClick()
    {
        Debug.Log("Pressed Quests");
    }

    public void OnShopButtonClick()
    {
        Debug.Log("Pressed Shop");
    }

    public void OnOptionsButtonClick()
    {
        Debug.Log("Pressed Options");
    }

    public void OnPlayerButtonClick()
    {
        Debug.Log("Pressed Player");
    }

    public void OnFacebookButtonClick()
    {
        Debug.Log("Pressed Facebook");
    }

    public void OnInfoButtonClick()
    {
        Debug.Log("Pressed Info");
    }
}
