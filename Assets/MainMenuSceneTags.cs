﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuSceneTags : MonoBehaviour {
        protected const string TEXTBOX_TAG = "TextBox";
        protected const string LAYOUT_TAG = "Layout";
        protected static GameObject[] textBoxes;
        protected static GameObject[] layouts;

        //it is a separate function instead setting value in each textbox cause using this method we can manage size of all textboxes only by changing 1 variable in 1 script
        public static void FontSizeAdapter()
        {
            if (textBoxes == null)
            {
            textBoxes = GameObject.FindGameObjectsWithTag(TEXTBOX_TAG);   //searching all textboxes
            }
            foreach (GameObject text in textBoxes)
            {
                text.GetComponentInChildren<Text>().fontSize = MainMenuSceneInit.fontSize;    //changing size everywhere
            }
        }

        public static void LayoutSpacingAdapter()
        {
            if(layouts == null)
            {
            layouts = GameObject.FindGameObjectsWithTag(LAYOUT_TAG);      //searching all layouts
            }
            foreach (GameObject layout in layouts)
            {
                if (layout.GetComponent<HorizontalLayoutGroup>())           //some of them have only horizontal or vertaical layouts
                {          
                layout.GetComponent<HorizontalLayoutGroup>().spacing = MainMenuSceneInit.spacingSize;
                }
                if (layout.GetComponent<VerticalLayoutGroup>())
                {
                layout.GetComponent<VerticalLayoutGroup>().spacing = MainMenuSceneInit.spacingSize;
                }
            }
        }
}
